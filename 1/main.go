package main

import (
	"fmt"
	"math"
)

func isMultipleOf(n, of int) bool {
	if math.Mod(float64(n), float64(of)) == 0 {
		return n > 0
	}
	return false
}

func main() {
	var sum int
	for i := 0; i < 1000; i++ {
		if isMultipleOf(i, 3) || isMultipleOf(i, 5) {
			sum += i
		}
	}
	fmt.Printf("%v\n", sum)
}
