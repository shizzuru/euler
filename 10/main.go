package main

import (
	"fmt"
	"math"
)

// From challenge #3
// https://en.wikipedia.org/wiki/Primality_test
func isPrime(n int) bool {
	if n <= 3 {
		return n > 1
	} else if math.Mod(float64(n), 2) == 0 || math.Mod(float64(n), 3) == 0 {
		return false
	}
	for i := 5; i*i <= n; i += 6 {
		if math.Mod(float64(n), float64(i)) == 0 || math.Mod(float64(n), float64(i+2)) == 0 {
			return false
		}
	}
	return true
}

func findNextPrime(from int) int {
	from++
	for !isPrime(from) {
		from++
	}
	return from
}

func solve() (sum int) {
	var i int
	// This looks weird but it's more efficient than using "for findNextPrime(i)" only to assign findNextPrime to i later on. It saves time and reduces complexity
	// I'll surely find a more elegant way to do it later
	for true {
		if j := findNextPrime(i); j > eulerNb {
			break
		} else {
			sum += j
			i = j
		}
	}
	return
}

const eulerNb = 2000000

func main() {
	fmt.Printf("%v\n", solve())
}
