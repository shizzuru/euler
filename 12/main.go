package main

import (
	"fmt"
	"time"
)

// My first recursive function ever BOISSSS
func triangleSequence(nth int64) int64 {
	if nth < 2 {
		return nth
	}
	return nth + triangleSequence(nth-1)
}

func factorsOf(in int64) (rs []int64) {
	for i := int64(1); i <= in/2; i++ { // sooooo efficient
		if in%i == 0 {
			rs = append(rs, i)
		}
	}
	return append(rs, in) // soooo beautiful
}

func solve() (i int64) {
	// for int64(len(factorsOf(triangleSequence(i)))) < eulerNb {
	// 	i++
	// }
	i = 75000000
	for true {
		ts := triangleSequence(i)
		le := int64(len(factorsOf(ts)))
		if le < eulerNb {
			fmt.Printf("ts : %v - len : %v\n", ts, le)
		}
		i++
	}
	return triangleSequence(i) // Trading complexity for space. Bad bad trade.
}

var eulerNb int64 = 500

func main() {
	t := time.Now()
	fmt.Printf("%v\n", solve())
	fmt.Printf("Computed in %v", time.Since(t))
}
