package main

import "fmt"

func isEven(in uint) bool {
	if in%2 == 0 {
		return true
	}
	return false
}

func collatzSequence(in uint) (rs []uint) {
	rs = append(rs, in)
	for in > 1 {
		if isEven(in) {
			in = in / 2
			rs = append(rs, in)
		} else {
			in = 3*in + 1
			rs = append(rs, in)
		}
	}
	return
}

func solve() (rs uint) {
	var maxLength uint
	for i := 0; i < 1000000; i++ {
		if cs := uint(len(collatzSequence(uint(i)))); cs > maxLength {
			rs = uint(i)
			maxLength = cs
		}
	}
	return
}

func main() {
	fmt.Printf("%v\n", solve())
}
