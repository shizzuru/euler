package main

import (
	"fmt"
	"math"
)

// Really interesting problem. Its solutions ->
// https://en.wikipedia.org/wiki/Lattice_path
// https://en.wikipedia.org/wiki/Binomial_coefficient
// https://www.robertdickau.com/lattices.html

// eulerNB = 20 -> overflows left and right
// Must refactor and use bigint

func printLattice(lattice [][]int) {
	for _, v := range lattice {
		for _, v2 := range v {
			fmt.Printf("%v ", v2)
		}
		fmt.Printf("\n")
	}
}

func initGrid(g [][]int) {
	for i := range g {
		g[i][0] = 1
	}
	for i := range g[0] {
		g[0][i] = 1
	}
}

func calcLatticePath(g [][]int) {
	for i, v := range g {
		if i == 0 {
			continue
		}
		for j := range v {
			if j == 0 {
				continue
			}
			g[i][j] = g[i][j-1] + g[i-1][j]
		}
	}
}

func weirdSolve() {
	grid := make([][]int, eulerNb)
	for i := range grid {
		grid[i] = make([]int, eulerNb)
	}
	initGrid(grid)
	calcLatticePath(grid)
	printLattice(grid)

	fmt.Printf("\nSolution : %v\n", grid[len(grid)-1][len(grid[len(grid)-1])-1])
}

func factorial(in uint64) uint64 {
	if in > 1 {
		return in * factorial(in-1)
	}
	return 1
}

func solve() uint64 {
	a := factorial(2 * eulerNb)
	t := uint64(math.Pow(float64(factorial(eulerNb)), 2))
	fmt.Printf("a : %v\nt : %v\n", a, t)
	rs := a / t
	return rs
}

const eulerNb = 20

func main() {
	fmt.Printf("%v\n", solve())
	fmt.Printf("%v\n", factorial(20))
}
