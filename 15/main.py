import math

eulerNb = 20

# Apparently i shouldn't use math.pow here as i'm using integers
# https://docs.python.org/2/library/math.html#math.pow
rs = math.factorial(2*eulerNb) // math.factorial(eulerNb)**2

print(rs)
#Gosh python is so great for maths