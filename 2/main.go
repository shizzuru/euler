package main

import "fmt"

func fibo() func() int {
	a, b := 1, 1
	return func() int {
		a, b = b, a+b
		return a
	}
}

func isEven(n int) bool {
	if n%2 == 0 {
		return true
	}
	return false
}

func main() {
	var sum int
	fib := fibo()
	for f := fib(); f < 4000000; f = fib() {
		if isEven(f) {
			sum += f
		}
	}
	fmt.Printf("%v\n", sum)
}
