package main

import (
	"fmt"
	"math"
)

// https://en.wikipedia.org/wiki/Primality_test
func isPrime(n int) bool {
	if n <= 3 {
		return n > 1
	} else if math.Mod(float64(n), 2) == 0 || math.Mod(float64(n), 3) == 0 {
		return false
	}
	for i := 5; i*i <= n; i += 6 {
		if math.Mod(float64(n), float64(i)) == 0 || math.Mod(float64(n), float64(i+2)) == 0 {
			return false
		}
	}
	return true
}

func biggestPrimeFactorOf(of int) int {

}

var eulerNb = 600851475143

func main() {
	fmt.Printf("%v\n", biggestPrimeFactorOf(eulerNb))
}
