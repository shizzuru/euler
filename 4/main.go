package main

import (
	"fmt"
	"strconv"
)

func isPalindrome(in []byte) bool {
	for i, v := range in[:len(in)/2] {
		if v != in[len(in)-(i+1)] {
			return false
		}
	}
	return true
}

func solve() (solution int) {
	for i := 0; i < 999; i++ {
		for j := 0; j < 999; j++ {
			if rs := i * j; isPalindrome([]byte(strconv.Itoa(rs))) && rs > solution {
				solution = rs
			}
		}
	}
	return
}

func main() {
	fmt.Printf("%v\n", solve())
}
