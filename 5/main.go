package main

import (
	"fmt"
)

func evenlyDivTo(in, to int) bool {
	for i := 1; i <= to; i++ {
		if in%i != 0 || in < to {
			return false
		}
	}
	return true
}

func solve(to int) int {
	var i int
	for !evenlyDivTo(i, to) {
		i++
	}
	return i
}

func main() {
	fmt.Printf("%v\n", solve(20))
}
