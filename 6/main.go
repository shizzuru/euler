package main

import (
	"fmt"
	"math"
)

func sumOfSquaredNumbers(to int) (sum int) {
	for i := 1; i <= to; i++ {
		sum += int(math.Pow(float64(i), 2.))
	}
	return
}

func squareOfSum(to int) int {
	var rs int
	for i := 1; i <= to; i++ {
		rs += i
	}
	return int(math.Pow(float64(rs), 2.))
}

func main() {
	for i := 1; i <= 100; i++ {
		fmt.Printf("%v : %v\n", i, squareOfSum(i)-sumOfSquaredNumbers(i))
	}
}
